package homework05;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    /*static {
        String className = Thread.currentThread()
                .getStackTrace()[1]
                .getClassName();
        System.out.printf("Loading class %s%n",className);
    }
    {
        System.out.printf("Create object %s%n",this.getClass().getCanonicalName());
    }*/

    private final Human mother;
    private final Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        //единственным условием создания новой семьи является наличие 2-х родителей,
        // при этом у родителей должна устанавливаться ссылка на текущую новую семью, а семья создается с пустым массивом детей.
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
        this.children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public boolean addChild(Human child){

        if (getIndexChild(child) != -1) return false;

        child.setFamily(this);
        children = Arrays.copyOf(children,children.length+1);
        children[children.length-1] = child;

        return true;
    }

    public int countFamily(){
        int countParent = 2;
        int countChildren = children.length;
        return countParent + countChildren;
    }

    public Human getChild(int indexChild){

        return (indexIsValid(indexChild)) ? children[indexChild] : null;

    }
    public int getIndexChild(Human child){

        return Arrays.asList(children).indexOf(child);
    }

    public boolean deleteChild(int indexChild){

        if (!indexIsValid(indexChild)) return false;

        Human child = children[indexChild];
        child.setFamily(null);

        Human[] newChildren= new Human[children.length - 1];

        System.arraycopy(children, 0, newChildren, 0, indexChild);
        System.arraycopy(children, indexChild + 1, newChildren, indexChild, children.length - indexChild - 1);

        children = newChildren;

        return true;
    }


    public boolean deleteChild(Human child){
        return deleteChild(getIndexChild(child));
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return String.format("Family{\n mother=%S,\n father=%S,\n children=%S,\n  pet=%S }",
                mother.toString(),
                father.toString(),
                Arrays.toString(children),
                (pet==null) ?("null"):(pet.toString()));
    }

    @Override
    protected void finalize()  {
        System.out.printf("Delete object %s%n",this.getClass().getCanonicalName());
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    private boolean indexIsValid(int indexChild) {
        return indexChild >= 0 && indexChild < children.length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (this.hashCode() == o.hashCode())  return true;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}
