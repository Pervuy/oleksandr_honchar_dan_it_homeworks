package homework05;

import libs.Console;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {

   /* static {
        String className = Thread.currentThread()
                .getStackTrace()[1]
                .getClassName();
        System.out.printf("Loading class %s%n",className);
    }
    {
        System.out.printf("Create object %s%n",this.getClass().getCanonicalName());
    }*/

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq,  String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        setIq(iq);
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {
    }

    public void greetPet(){
        if (family != null && family.getPet() != null){
            System.out.printf("Привет, %s\n",this.family.getPet().getNickname());
        }
    }
    public void describePet(){
        if (family != null && family.getPet() != null){
            System.out.printf("У меня есть %s, ему %d лет, он %s\n",this.family.getPet().getSpecies(),this.family.getPet().getAge(),this.family.getPet().getDescribeTrick());

        }else {
            Console.println("У мене нету питомца");
        }
    }

    public boolean feedPet(boolean isTimeFeed){

        if (family==null) return false;
        if (family.getPet() == null) return  false;

        if (isTimeFeed){
            System.out.printf("Хм... покормлю ка я %s%n",family.getPet().getNickname());
            return true;
        }else{
            int trickLevelPet = family.getPet().getTrickLevel();
            Random random = new Random();
            int trickLevelRandom = random.nextInt(101);
            if (trickLevelPet > trickLevelRandom){
                System.out.printf("Хм... покормлю ка я %s%n",family.getPet().getNickname());
                return true;
            }else {
                System.out.printf("Думаю, %s не голоден.%n",family.getPet().getNickname());
                return false;
            }
        }
    }

    @Override
    public String toString() {

        //Human{name='Name', surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}",
                name,
                surname,
                year,
                iq,
                Arrays.deepToString(schedule));
    }

    public String getName() {
        return String.format("%s %s",name, surname);
    }

    public void setIq(int iq) {
        if (iq < 0 || iq > 100) {
            throw new IllegalArgumentException("Уровень IQ должен быть в диапазоне от 0 до 100.");
        }
        this.iq = iq;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object anObject) {
        if (this == anObject) return true;
        if (anObject == null || getClass() != anObject.getClass()) return false;
        if (this.hashCode() == anObject.hashCode())  return true;
        Human human = (Human) anObject;

        return year == human.year && iq == human.iq && name.equals(human.name) && surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    public Family getFamily() {
        return family;
    }
    public Human getFather(){
        if(family==null) return null;
        return family.getFather();
    }
    public Human getMother(){
        if(family==null) return null;
        return family.getMother();
    }

    @Override
    protected void finalize()  {
        System.out.printf("Finalize object %s%n",this.toString());
    }
}
