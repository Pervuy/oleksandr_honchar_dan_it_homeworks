package homework05;

import libs.Console;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    static {
        String className = Thread.currentThread()
                .getStackTrace()[1]
                .getClassName();
        System.out.printf("Loading class %s%n",className);
    }
    {
        System.out.printf("Create object %s%n",this.getClass().getCanonicalName());
    }

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        setTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public void eat(){
        Console.println("Я кушаю!");
    }
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }
    public void foul(){
        Console.println("Нужно хорошо замести следы...");
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public String getDescribeTrick() {
        return (this.trickLevel <= 50?("почти не хитрый"):("очень хитрый"));
    }

    public String[] getHabits() {
        return habits;
    }

    @Override
    public String toString() {
       // dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=%s, canFly=%b, hasFur=%b, numberOfLegs=%d}",
                species,
                nickname,
                age,
                trickLevel,
                Arrays.toString(habits),
                species.isCanFly(),
                species.isHasFur(),
                species.getNumberOfLegs());

    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel < 0 || trickLevel > 100) {
            throw new IllegalArgumentException("Уровень хитрости должен быть в диапазоне от 0 до 100.");
        }
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    protected void finalize()  {
        System.out.printf("Delete object %s%n",this.getClass().getCanonicalName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (this.hashCode() == o.hashCode())  return true;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && species == pet.species && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
}
