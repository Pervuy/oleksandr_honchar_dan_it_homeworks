package homework05;

public enum Species {

    DOG(false,true,4),
    PARROT(true,false,2),
    CAT(false,true,4);

    private final boolean canFly;
    private final boolean hasFur;
    private final int numberOfLegs;

    Species(boolean canFly, boolean hasFur, int numberOfLegs){
        this.canFly = canFly;
        this.hasFur = hasFur;
        this.numberOfLegs = numberOfLegs;
    }

    public boolean isCanFly(){
        return this.canFly;
    }

    public boolean isHasFur(){
        return  this.hasFur;
    }

    public int getNumberOfLegs(){
        return this.numberOfLegs;
    }

}
