package homework08.service;

import homework08.Family;
import homework08.Human;
import homework08.Pet;
import homework08.dao.CollectionFamilyDao;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {

    private final CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int countFamily) {

        return getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() > countFamily)
                .toList();
    }

    public List<Family> getFamiliesLessThan(int countFamily) {

        return getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() < countFamily)
                .toList();
    }
    public void displayAllFamilies() {
        //вывести на экран все семьи (в индексированном списке) со всеми членами семьи.
        getAllFamilies().forEach(System.out::println);
    }

    public void displayFamiliesBiggerThan(int countFamily) {
        // найти семьи с количеством людей больше чем (принимает количество человек и возвращает все семьи,
        // где количество людей больше чем указанное); выводит информацию на экран.
        getFamiliesBiggerThan(countFamily).forEach(System.out::println);
    }

    public void displayFamiliesLessThan(int countFamily){
        // найти семьи с количеством людей меньше чем (принимает количество человек и возвращает все семьи,
        // где количество людей меньше чем указанное); выводит информацию на экран.
        getFamiliesLessThan(countFamily).forEach(System.out::println);
    }

    public int countFamiliesWithMemberNumber(int memberNumber) {
        //- подсчитать число семей с количеством людей равное переданному числу.
        return (int) getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() == memberNumber)
                .count();
    }

    public void createNewFamily(Human mother, Human father) {
        // - создать новую семью (принимает 2 параметра типа Human) - создает новую семью, сохраняет в БД.
        familyDao.saveFamily(new Family(mother, father));
    }
    public void saveFamily(Family family){
        familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex(int index) {
        //- удалить семью по индексу в списке - удаляет семью из БД.
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        //- родить семьей ребенка (принимает Family и 2 типа String: мужское и женское имя) -
        // в данной семье появляется новый ребенок с учетом данных родителей, информация о семье обновляется в БД;
        // метод возвращает обновленную семью. Если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское.
        family.bornChild(nameMan,nameWoman);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        //- усыновить ребенка (принимает 2 параметра: Family, Human)- в данной семье сохраняется данный ребенок,
        // информация о семье обновляется в БД; метод возвращает обновленную семью.
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }
    public void deleteAllChildrenOlderThen(int age) {
        // удалить детей старше чем (принимает int) - во всех семьях удаляются дети,
        // которые старше указанно возраста, информация обновляется в БД.

        /*List<Family> allFamilies = getAllFamilies();
        for (Family family : allFamilies) {
            ArrayList<Human> newChildren = new ArrayList<>();
            family.getChildren()
                    .stream()
                    .filter(child -> child.getAge() < age).forEach(newChildren::add);
            family.setChildren(newChildren);
            familyDao.saveFamily(family);
        }*/

        getAllFamilies().forEach(family -> {
            List<Human> newChildren = family.getChildren().stream()
                    .filter(child -> child.getAge() < age)
                    .collect(Collectors.toList());
            family.setChildren(newChildren);
            familyDao.saveFamily(family);
        });
    }
    public int count() {
        //- возвращает количество семей в БД.
        return familyDao.count();
    }

    public Family getFamilyById(int idFamily) {
        //- принимает индекс семьи, возвращает Family по указанному индексу.
        return familyDao.getFamilyByIndex(idFamily);
    }

    public Set<Pet> getPets(int idFamily) {
        // - принимает индекс семьи, возвращает список домашних животных, которые живут в семье.
        return familyDao.getFamilyByIndex(idFamily).getPets();
    }

    public void addPet(int idFamily, Pet pet) {
        // принимает индекс семьи и параметр Pet - добавляет нового питомца в семью, обновляет данные в БД.
        Family familyByIndex = familyDao.getFamilyByIndex(idFamily);
        familyByIndex.addPet(pet);
        familyDao.saveFamily(familyByIndex);
    }


}
