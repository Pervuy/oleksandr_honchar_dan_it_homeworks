package homework08.dao;

import homework08.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int id);
    boolean deleteFamily(int id);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);

}
