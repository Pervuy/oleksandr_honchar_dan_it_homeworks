package homework08;

public interface HumanCreator {

    Human bornChild(String nameMan, String nameWoman);
}
