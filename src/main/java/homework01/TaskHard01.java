package homework01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class TaskHard01 {
    public static void main(String[] args) throws IOException {

        String [][] famousEvent = creatFamousEventArray();
        int randomIndex = new Random().nextInt(famousEvent.length);
        int randomNumber = Integer.parseInt(famousEvent[randomIndex][0]);
        int currentNumber;
        Array array = new Array();
        System.out.println("Write your name, please.");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String name = bufferedReader.readLine();

        System.out.println("Let the game begin!");

        while (true){
            System.out.println("What year did "+famousEvent[randomIndex][1]);


            try {
                currentNumber = Integer.parseInt(bufferedReader.readLine());

                if (currentNumber == randomNumber){
                    array.sort();
                    System.out.println("Your numbers: " + array.toString());
                    System.out.println("Congratulations, "+name +"!");
                    break;
                }else if (currentNumber < randomNumber){
                    array.add(currentNumber);
                    System.out.println("Your year is too small. Please, try again.");
                } else if (currentNumber > randomNumber) {
                    array.add(currentNumber);
                    System.out.println("Your year is too big. Please, try again.");
                }

            }catch (NullPointerException e){
                //if the user entered not number, then enter number again
                continue;
            }
        }
    }
    private static String [][] creatFamousEventArray(){
        String [][] creatFamous = new String[16][2];

        creatFamous = new String[][]{{"1939", "World War II"},
                        {"1969", "First manned mission to land on the Moon"},
                        {"2001", "Terrorist attacks on the World Trade Center"},
                        {"1989", "The wall separating East and West Berlin fell"},
                        {"1986", "Nuclear disaster at the Chernobyl Nuclear Power Plant"},
                        {"2004", "Deadly tsunami caused by an earthquake"},
                        {"1918", "Global pandemic that infected an estimated 500 million people"},
                        {"1789", "Revolution that led to the overthrow of the French monarchy"},
                        {"1815", "Famous battle between Napoleon and the British"},
                        {"1955", "War between North and South Vietnam and their allies"},
                        {"1929", "Severe economic downturn that lasted for a decade"},
                        {"1917", "Revolution that led to the rise of the Soviet Union"},
                        {"1347", "Pandemic that killed an estimated 75-200 million people"},
                        {"1945", "Hiroshima and Nagasaki, Japan', 'Atomic bombings that ended World War II"},
                        {"1492", "Christopher Columbus discovered the New World"},
                        {"1861", "War between the Northern and Southern states"}};
        return creatFamous;

              // (1, 'World War II', '1939-09-01', 'Europe', 'A global war that lasted from 1939 to 1945.'),
      //  (2, 'Apollo 11 Moon Landing', '1969-07-20', 'Moon', 'First manned mission to land on the Moon.'),
       // (3, '9/11 Attacks', '2001-09-11', 'New York City, USA', 'Terrorist attacks on the World Trade Center.'),
      //  (4, 'Fall of the Berlin Wall', '1989-11-09', 'Berlin, Germany', 'The wall separating East and West Berlin fell.'),
       // (5, 'Chernobyl Disaster', '1986-04-26', 'Chernobyl, Ukraine', 'Nuclear disaster at the Chernobyl Nuclear Power Plant.'),
       // (6, 'Indian Ocean Tsunami', '2004-12-26', 'Indian Ocean', 'Deadly tsunami caused by an earthquake.'),
      //  (7, 'Spanish Flu', '1918-01-01', 'Worldwide', 'Global pandemic that infected an estimated 500 million people.'),
      //  (8, 'French Revolution', '1789-07-14', 'France', 'Revolution that led to the overthrow of the French monarchy.'),
       // (9, 'Battle of Waterloo', '1815-06-18', 'Waterloo, Belgium', 'Famous battle between Napoleon and the British.'),
       // (10, 'Vietnam War', '1955-11-01', 'Vietnam', 'War between North and South Vietnam and their allies.'),
       // (11, 'Great Depression', '1929-10-29', 'Worldwide', 'Severe economic downturn that lasted for a decade.'),
       // (12, 'Russian Revolution', '1917-10-25', 'Russia', 'Revolution that led to the rise of the Soviet Union.'),
       // (13, 'Black Death', '1347-01-01', 'Worldwide', 'Pandemic that killed an estimated 75-200 million people.'),
       // (14, 'Hiroshima and Nagasaki Bombings', '1945-08-06', 'Hiroshima and Nagasaki, Japan', 'Atomic bombings that ended World War II.'),
       // (15, 'Columbus\'s Discovery of America', '1492-10-12', 'Caribbean', 'Christopher Columbus discovered the New World.'),
       // (16, 'American Civil War', '1861-04-12', 'USA', 'War between the Northern and Southern states.'),
    }
}
