package homework01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Task01 {
    public static void main(String[] args) throws IOException {
        int randomNumber = new Random().nextInt(101);
        int currentNumber;
        System.out.println("Write your name, please.");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String name = bufferedReader.readLine();

        System.out.println("Let the game begin!");

        while (true){
            System.out.println("Enter number between 0 and 100");


            try {
                currentNumber = Integer.parseInt(bufferedReader.readLine());

                if (currentNumber == randomNumber){
                    System.out.println("Congratulations, "+name +"!");
                    break;
                }else if (currentNumber < randomNumber){
                    System.out.println("Your number is too small. Please, try again.");
                } else  {
                    System.out.println("Your number is too big. Please, try again.");
                }

            }catch (NullPointerException e){
                //if the user entered not number, then enter number again
                continue;
            }


        }
    }
}
