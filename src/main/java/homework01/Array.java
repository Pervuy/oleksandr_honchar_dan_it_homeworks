package homework01;

import java.util.Arrays;

public class Array {

    private static final int DEFAULT_CAPACITY = 4;

    private int[] data;
    private int next;

    public Array(){
        this(DEFAULT_CAPACITY);
    }
    public Array(int capacity){
        data = new int[capacity];
        next = 0;
    }

    private int calcNewSize(){

        return (int) (next + next * 1.5 + 1);
    }
    private void grow(){

        data = Arrays.copyOf(data, calcNewSize());
    }

    private void ensureSizeEnough(){
        if (next >= data.length) grow();
    }

    public void add(int newElement){
        ensureSizeEnough();
        data[next] = newElement;
        next ++;

        //throw libs.libs.EX.NI;                ;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < next; i++) {
            if (i != 0) sb.append(", ");
            sb.append(data[i]);
        }
        sb.append("]");
        return sb.toString();
    }
    public void sort(){
        Arrays.sort(data,0,next);
    }
}
