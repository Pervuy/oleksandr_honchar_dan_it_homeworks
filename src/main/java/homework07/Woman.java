package homework07;

import libs.Console;

import java.util.Map;

public final class Woman extends Human{

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman() {
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.printf("Hello, my dear %s\n", pet.getNickname());

    }
    @Override
    public String toString() {
        return String.format("Woman{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getYear(),
                super.getIq(),
                super.getSchedule().toString());
    }

    public void makeup(){
        Console.println("I like makeup!");
    }
}
