package homework07;

import libs.Console;

import java.util.Map;

public final class Man extends Human{
    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man() {
    }

   @Override
    public void greetPet(Pet pet) {
       System.out.printf("Hi, %s\n", pet.getNickname());
    }

    @Override
    public String toString() {
        return String.format("Man{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getYear(),
                super.getIq(),
                super.getSchedule().toString());
    }

    public void repairCar(){
        Console.println("I like repair car");
    }
}
