package homework07;

import java.util.*;

public class Family implements HumanCreator{

    private final Human mother;
    private final Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        //единственным условием создания новой семьи является наличие 2-х родителей,
        // при этом у родителей должна устанавливаться ссылка на текущую новую семью, а семья создается с пустым массивом детей.
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = new TreeSet<>();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public boolean addChild(Human child){

        if (getIndexChild(child) != -1) return false;
        child.setFamily(this);
        children.add(child);

        return true;
    }

    public int countFamily(){
        int countParent = 2;
        int countChildren = children.size();
        return countParent + countChildren;
    }

    public Human getChild(int indexChild){

        return (indexIsValid(indexChild)) ? children.get(indexChild) : null;

    }
    public int getIndexChild(Human child){

        return children.indexOf(child);
    }

    public boolean deleteChild(int indexChild){

        if (!indexIsValid(indexChild)) return false;

        Human child = children.get(indexChild);
        child.setFamily(null);
        children.remove(indexChild);

        return true;
    }

    public boolean deleteChild(Human child){
        return deleteChild(getIndexChild(child));
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return String.format("Family{\n mother=%S,\n father=%S,\n children=%S,\n  pet=%S}",
                mother.toString(),
                father.toString(),
                children.toString(),
                pets.toString());
    }

    @Override
    protected void finalize()  {
        System.out.printf("Delete object %s%n",this.getClass().getCanonicalName());
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    private boolean indexIsValid(int indexChild) {
        return indexChild >= 0 && indexChild < children.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (this.hashCode() == o.hashCode())  return true;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    public Human bornChild() {
        /*Реализация должна возвращать новый объект Man или Woman, с установленными ссылками на текущую семью,
        фамилией отца, случайным именем (нужно заранее создать список имен), и средним IQ (от отца и матери).
        Пол ребенка определяется случайно с вероятностью 50х50.
         */
        Human newChild = determineGender();
        newChild.setSurname(this.father.getSurname());
        newChild.setName(getNewName(newChild));
        newChild.setIq(getAverageIqMotherAndFather());

        addChild(newChild);
        return newChild;
    }

    private Human determineGender() {
        int randomGender = new Random().nextInt(2);
        return (randomGender == 0) ? (new Woman()):(new Man());
    }
    private String getNewName(Human child){
       if (child instanceof Man){
           List<String> listName = getListManName();
           return  listName.get(new Random().nextInt(listName.size()));
       } else {
           List<String> listName = getListWomanName();
           return  listName.get(new Random().nextInt(listName.size()));
       }
    }

    private int getAverageIqMotherAndFather(){
        return ((father.getIq()+mother.getIq()) / 2);
    }

    private static List<String> getListManName(){

        return Arrays.asList("Sasha","Nik","Serg","Andrey","Mihail","Dima");
    }
    private static List<String> getListWomanName(){

        return Arrays.asList("Masha","Nika","Katia","Pocahontas","Lena","Svetlana");
    }
}
