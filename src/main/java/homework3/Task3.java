package homework3;

import libs.Console;
import libs.ScannerConstrained;

public class Task3 {

    private static final String[][] schedule = new String[7][2];

    public static void main(String[] args) {
        String dayOfWeek;
        String command;

        fillShedule();

        while (true) {

            Console.println("Please, input the day of the week: \n");
            String usersCommand = new ScannerConstrained().nextLine();

            if (usersCommand.equals("exit")) break;

            String[] commandAndDay = usersCommand.trim().split(" ");
            if (commandAndDay.length == 1) {
                dayOfWeek = commandAndDay[0];
                printTaskForDay(dayOfWeek);
            } else if (commandAndDay.length == 2) {
                command = commandAndDay[0];
                dayOfWeek = commandAndDay[1];

                if (command.equals("change")||command.equals("reschedule")){
                    changeTaskForDay(dayOfWeek);
                }else {
                    Console.println("Sorry, I don't understand you, please try again.");
                }
            }
        }
    }

    private static void changeTaskForDay(String dayOfWeek) {
        String newTask;
        switch (DayOfWeek.fromValue(dayOfWeek)) {
            case SUNDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[0][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[0][1] = newTask;
            }
            case MONDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[1][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[1][1] = newTask;
            }
            case TUESDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[2][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[2][1] = newTask;
            }
            case WEDNESDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[3][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[3][1] = newTask;
            }
            case THURSDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[4][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[4][1] = newTask;
            }
            case FRIDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[5][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[5][1] = newTask;
            }
            case SATURDAY -> {
                System.out.printf("Please, input new tasks for %s\n", schedule[6][0]);
                newTask = new ScannerConstrained().nextLine();
                schedule[6][1] = newTask;
            }
            default -> Console.println("Sorry, I don't understand you, please try again.");
        }
    }

    private static void fillShedule() {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "study the course; read literatures for java";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to the work; read literatures for java; do home work";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to the work; study the course; do home work";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to the work; read literatures for java; do home work";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to the work; study the course; do home work";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to the work; study the course; drink beer";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "free day";
    }

    private static void printTaskForDay(String dayOfWeek) {

        switch (DayOfWeek.fromValue(dayOfWeek)) {
            case SUNDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[0][0], schedule[0][1]);
            case MONDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[1][0], schedule[1][1]);
            case TUESDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[2][0], schedule[2][1]);
            case WEDNESDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[3][0], schedule[3][1]);
            case THURSDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[4][0], schedule[4][1]);
            case FRIDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[5][0], schedule[5][1]);
            case SATURDAY -> System.out.printf("Your tasks for %s: %s\n", schedule[6][0], schedule[6][1]);
            default -> Console.println("Sorry, I don't understand you, please try again.");
        }
    }
}
