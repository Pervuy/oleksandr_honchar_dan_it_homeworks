package homework3;

public enum DayOfWeek {

    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    NOT_FOUND("Not found");

    private final String value;

    DayOfWeek(final String value) {
        this.value = value;
    }

    public static DayOfWeek fromValue(String value) {
        for (final DayOfWeek dayOfWeek : values()) {
            if (dayOfWeek.value.equalsIgnoreCase(value.trim())) {
                return dayOfWeek;
            }
        }
        return NOT_FOUND;
    }
}

