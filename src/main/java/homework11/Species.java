package homework11;

public enum Species {

    DOG(false,true,4),
    FISH(false,false,0),
    DOMESTIC_CAT(false,true,4),
    UNKNOWN;

    private boolean canFly;
    private boolean hasFur;
    private int numberOfLegs;

    Species(boolean canFly, boolean hasFur, int numberOfLegs){
        this.canFly = canFly;
        this.hasFur = hasFur;
        this.numberOfLegs = numberOfLegs;
    }

    Species(){

    }

    public boolean isCanFly(){
        return this.canFly;
    }

    public boolean isHasFur(){
        return  this.hasFur;
    }

    public int getNumberOfLegs(){
        return this.numberOfLegs;
    }
    public static Species getValue(Pet pet){
       if (pet instanceof Dog){
           return DOG;
       } else if (pet instanceof DomesticCat) {
           return DOMESTIC_CAT;
       } else if (pet instanceof Fish) {
           return FISH;
       }else {
           return UNKNOWN;
       }
    }

}
