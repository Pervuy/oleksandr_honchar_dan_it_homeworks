package homework11.controller;

import homework11.*;
import homework11.menu.Menu;
import homework11.menu.MenuCommands;
import homework11.service.FamilyService;
import libs.Console;
import libs.ScannerConstrained;

import java.io.*;
import java.util.List;

import static homework11.menu.MenuCommands.EDIT_FAMILY_BY_INDEX;

public class FamilyController {
    private final FamilyService familyService;
    private ScannerConstrained sc = new ScannerConstrained();

    public FamilyController() {
        this.familyService = new FamilyService();
    }

    public void runContextMenu(MenuCommands menuCommandsByIndex) {

        switch (menuCommandsByIndex){
            case CREATE_TEST_DATA -> createTestData();
            case SHOW_ALL_FAMILY_LIST  -> displayAllFamilies();
            case SHOW_FAMILIES_BIGGER_THAN -> displayFamiliesBiggerThan();
            case SHOW_FAMILIES_LESS_THAN -> displayFamiliesLessThan();
            case COUNT_FAMILIES_WITH_MEMBER_NUMBER -> countFamiliesWithMemberNumber();
            case CREATE_NEW_FAMILY -> createNewFamily();
            case DELETE_FAMILY_BY_INDEX -> deleteFamilyByIndex();
            case EDIT_FAMILY_BY_INDEX -> editFamilyByIndex();
            case DELETE_ALL_CHILDREN_OLDER_THEN -> deleteAllChildrenOlderThen();
            case BORN_CHILD -> bornChild();
            case ADOPT_CHILD -> adoptChild();
            case SAVE_IN_FILE -> saveInFile();
            case LOAD_FROM_FILE -> loadFromFile();
        }
        Console.print(">>>> ");


    }

    private void loadFromFile() {
        if (! new File("families.dat").exists()) return;

        try (ObjectInputStream ois = new ObjectInputStream(
                                                            new FileInputStream(
                                                                    new File("families.dat")))){


            List<Family> families = (List<Family>) ois.readObject();
            familyService.loadData(families);

        } catch (ClassNotFoundException | IOException e) {
            throw new RuntimeException(e);
        }
        //throw EX.NI;
    }

    private void saveInFile() {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("families.dat")))){

            oos.writeObject(familyService.getAllFamilies());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //throw EX.NI;
    }

    private void adoptChild() {
        /*  - запросить порядковый номер семьи (ID)
            - запросить необходимые данные (ФИО, год рождения, интеллект)*/
        int id = enterId();

        int countFamily = familyService.getFamilyById(id).countFamily();
        if (countFamily==Family.MAX_COUNT_FAMILY) throw new FamilyOverflowException();

        String childName = enterNameOrSurname("Введите имя ребенка");
        String childSurname = enterNameOrSurname("Введите фамилию ребенка");
        int childYear = enterYear();
        int childMonth = enterMonth();
        int childDay = enterDay(childMonth);
        int childIq = enterIq();
        Human child = new Human(childName, childSurname, String.format("%02d/%02d/%d",childDay,childMonth,childYear), childIq);
        familyService.adoptChild(familyService.getFamilyById(id),child);
    }

    private void bornChild() {
        /*  - запросить порядковый номер семьи (ID)
            - запросить необходимые данные (какое имя дать мальчику, какое девочке)*/
        int id = enterId();
        int countFamily = familyService.getFamilyById(id).countFamily();

        if (countFamily==Family.MAX_COUNT_FAMILY) throw new FamilyOverflowException();

        String boyName = enterNameOrSurname("Введите имя мальчика");
        String girlName = enterNameOrSurname("Введите фамилию девочки");

        familyService.bornChild(familyService.getFamilyById(id), boyName, girlName);
    }

    private void deleteAllChildrenOlderThen() {
        while (true){
            try {
                Console.println("Введите интересующий возраст");
                Console.print(">>>> ");
                int index = Integer.parseInt(sc.nextLine());
                familyService.deleteAllChildrenOlderThen(index);
                break;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }
        }
    }

    private void editFamilyByIndex() {
        if (familyService.count() != 0) {
            Menu contextMenu = new Menu(EDIT_FAMILY_BY_INDEX);
            while (true) {
                contextMenu.printMenu();
                Console.print(">>>> ");
                String stringLine = sc.nextLine();
                if (contextMenu.isNotValidIndex(stringLine)) continue;
                int indexMenu = Integer.parseInt(stringLine);
                MenuCommands menuCommandsByIndex = contextMenu.getMenuCommandsByIndex(indexMenu);
                this.runContextMenu(menuCommandsByIndex);
                break;
            }
        } else {
            Console.println("Список семей пуст");
        }

    }

    private void deleteFamilyByIndex() {
        if (familyService.count() != 0) {
            while (true) {
                try {
                    Console.println("Введите порядковый номер семьи (ID)");
                    Console.print(">>>> ");
                    int index = Integer.parseInt(sc.nextLine());
                    familyService.deleteFamilyByIndex(index);
                    break;
                } catch (NumberFormatException ex) {
                    Console.println("Введено не корректное число");
                }
            }
        } else {
            Console.println("Список семей пуст");
        }
    }

    private void createNewFamily() {
        /*- запросить имя матери
            - запросить фамилию матери
            - запросить год рождения матери
            - запросить месяц рождения матери
            - запросить день рождения матери
            - запросить iq матери

            - запросить имя отца
            - запросить фамилию отца
            - запросить год рождения отца
            - запросить месяц рождения отца
            - запросить день рождения отца
            - запросить iq отца*/
        String motherName = enterNameOrSurname("Введите имя матери");
        String motherSurname = enterNameOrSurname("Введите фамилию матери");
        int motherYear = enterYear();
        int motherMonth = enterMonth();
        int motherDay = enterDay(motherMonth);
        int motherIq = enterIq();

        Human mother = new Woman(motherName,motherSurname,String.format("%02d/%02d/%d",motherDay,motherMonth,motherYear),motherIq);

        String fatherName = enterNameOrSurname("Введите имя отца");
        String fatherSurname = enterNameOrSurname("Введите фамилию отца");
        int fatherYear = enterYear();
        int fatherMonth = enterMonth();
        int fatherDay = enterDay(fatherMonth);
        int fatherIq = enterIq();
        Human father = new Woman(fatherName,fatherSurname,String.format("%02d/%02d/%d",fatherDay,fatherMonth,fatherYear),fatherIq);

        familyService.createNewFamily(mother,father);

    }
    private String enterNameOrSurname(String massage){

        Console.println(massage);
        Console.print(">>>> ");

        return sc.nextLine();

    }

    private int enterId(){
        while (true){
            try {
                Console.println("Введите порядковый номер семьи (ID)");
                Console.print(">>>> ");
                int id = Integer.parseInt(sc.nextLine());
                if (id >= familyService.count() || id < 0) {
                    throw new IllegalArgumentException("Введен не корректный id");
                }
                return id;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }catch (IllegalArgumentException ex){
                Console.println(ex.getMessage());
            }
        }
    }

    private int enterYear(){
        while (true){
            try {
                Console.println("Введите год рождения");
                Console.print(">>>> ");

               return Integer.parseInt(sc.nextLine());

            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }
        }
    }

    private int enterMonth(){
        while (true){
            try {
                Console.println("Введите месяц рождения");
                Console.print(">>>> ");
                int month = Integer.parseInt(sc.nextLine());
                if ( month > 12 || month < 1){
                    throw new IllegalArgumentException("Месяц должен быть в диапазоне [1-12]");
                }
                return month;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }catch (IllegalArgumentException ex){
                Console.println(ex.getMessage());
            }
        }
    }
    private int enterDay(int month){
        int[] daysInMonth  = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int dayInCurrentMonth = daysInMonth[month - 1];
        while (true){

            try {
                Console.println("Введите день рождения");
                Console.print(">>>> ");
                int day = Integer.parseInt(sc.nextLine());
                if ( day > dayInCurrentMonth || day < 1){
                    throw new IllegalArgumentException(String.format("День должен быть в диапазоне [1-%d]",dayInCurrentMonth));
                }
                return day;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }catch (IllegalArgumentException ex){
                Console.println(ex.getMessage());
            }
        }
    }
    private int enterIq(){
        while (true){
            try {
                Console.println("Введите Iq");
                Console.print(">>>> ");
                int month = Integer.parseInt(sc.nextLine());
                if ( month > 100 || month < 1){
                    throw new IllegalArgumentException("Iq должен быть в диапазоне [1-100]");
                }
                return month;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }catch (IllegalArgumentException ex){
                Console.println(ex.getMessage());
            }
        }
    }

    private void countFamiliesWithMemberNumber() {
        while (true){
            try {
                Console.println("Введите интересующее число");
                Console.print(">>>> ");
                int countFamily = Integer.parseInt(sc.nextLine());
                Console.println(String.format("Таких семей %d",familyService.countFamiliesWithMemberNumber(countFamily)));
                break;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }
        }
    }

    private void displayFamiliesLessThan() {
        while (true){
            try {
                Console.println("Введите интересующее число");
                Console.print(">>>> ");
                int countFamily = Integer.parseInt(sc.nextLine());
                familyService.displayFamiliesLessThan(countFamily);
                break;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }
        }
    }

    private void displayFamiliesBiggerThan() {

        while (true){
            try {
                Console.println("Введите интересующее число");
                Console.print(">>>> ");
                int countFamily = Integer.parseInt(sc.nextLine());
                familyService.displayFamiliesBiggerThan(countFamily);
                break;
            }catch (NumberFormatException ex){
                Console.println("Введено не корректное число");
            }
        }
    }

    public void createTestData() {

        for (int i = 0; i < 15; i++) {
            int manYear = RandomData.getManYear();
            String manDayAndMonth = RandomData.getDayAndMonth();

            int womanYear = RandomData.getWomanYear(manYear);
            String womanDayAndMonth = RandomData.getDayAndMonth();

            Human man = new Man(RandomData.getManName(), RandomData.getManSurname(), String.format("%s/%d", manDayAndMonth, manYear), RandomData.getIq());
            Human woman = new Woman(RandomData.getWomanName(), man.getSurname(), String.format("%s/%d", womanDayAndMonth, womanYear), RandomData.getIq());
            Family family = new Family(woman, man);
            familyService.saveFamily(family);

        }
    }
    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }

}
