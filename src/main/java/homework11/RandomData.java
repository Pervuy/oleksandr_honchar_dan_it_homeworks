package homework11;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomData {
    public static String getManName(){
        List<String> listName = Arrays.asList("Ethan","Liam","Alexander","Benjamin","William","Christopher","Jacob",
                "Matthew","James","Nicholas","Daniel","Ryan","David","Joseph");
        return  listName.get(new Random().nextInt(listName.size()));
    }

    public static String getWomanName(){
        List<String> listName = Arrays.asList("Emma","Olivia","Sophia","Isabella","Ava","Mia","Charlotte","Amelia",
                "Harper","Abigail","Emily","Elizabeth","Madison","Sofia","Avery");
        return  listName.get(new Random().nextInt(listName.size()));
    }
    public static String getManSurname(){
        List<String> listSurname = Arrays.asList("Johnson","Smith","Brown","Garcia","Taylor","Davis","Miller","Wilson",
                "Anderson","Jackson","Lee","White","Harris","Martin","Clark");
        return listSurname.get(new Random().nextInt(listSurname.size()));
    }
    public static int getManYear(){
        // год рождения мужчины может быть сгенерирован с расчета, что ему должно быть уже 18 лет
        // и максимальное количество лет 100
        int max = LocalDate.now().getYear() - 18;
        int min = max - 82;
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }
    public static int getWomanYear(int manYear){
        // год рождения женщины может быть сгенерирован с расчета, что ей должно быть уже 16 лет
        // и максимальное количество лет 100, но при этом отклонение года мужчины не больше 10 лет
        int max = Math.min(manYear + 10, LocalDate.now().getYear() - 16);
        int min = Math.max(manYear - 10, max - 84) ;

        Random random = new Random();
        return  random.nextInt(max - min + 1) + min;
    }

    public static String getDayAndMonth(){
        int[] daysInMonth  = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        Random random = new Random();

        int month = random.nextInt(12) + 1;
        int day = random.nextInt(daysInMonth[month - 1]) + 1;

        return String.format("%02d/%02d", day, month);
    }

    public static int getIq(){
        Random random = new Random();
        return random.nextInt(100)+1;
    }

}
