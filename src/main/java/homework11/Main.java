package homework11;

import homework11.controller.FamilyController;
import homework11.menu.Menu;
import homework11.menu.MenuCommands;
import libs.Console;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Menu mainManu = new Menu();
        FamilyController familyController = new FamilyController();
        Scanner in = new Scanner(System.in);

        while (true){
            mainManu.printMenu();
            Console.print(">>>> ");
            String stringLine = in.nextLine();

            if (isExit(stringLine)) break;
            if (mainManu.isNotValidIndex(stringLine)) continue;
            int indexMenu = Integer.parseInt(stringLine);
            MenuCommands menuCommandsByIndex = mainManu.getMenuCommandsByIndex(indexMenu);
            familyController.runContextMenu(menuCommandsByIndex);
        }


    }

    private static boolean isExit(String stringLine) {
        return stringLine.toUpperCase().trim().equals("EXIT") || stringLine.trim().equals("12");
    }
}
