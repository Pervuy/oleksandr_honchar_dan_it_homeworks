package homework11;

import java.util.Set;

public class Fish extends Pet{

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish() {
    }

    @Override
    public void respond() {

    }
}
