package homework11;

public class FamilyOverflowException extends RuntimeException{

    public FamilyOverflowException() {
        super(String.format("Pазмер семьи больше %d",Family.MAX_COUNT_FAMILY));
    }
}
