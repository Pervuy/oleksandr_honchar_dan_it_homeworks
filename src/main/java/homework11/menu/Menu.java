package homework11.menu;


import libs.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static homework11.menu.MenuCommands.NOT_FOUND;

public class Menu {
    private final List<MenuCommands> menuCommandsList;

    public Menu(MenuCommands mainMenuCommand) {
       menuCommandsList = Arrays.stream(MenuCommands.values())
                .filter(contextMenu -> contextMenu.getParentMenu() == mainMenuCommand)
                .toList();
    }
    public Menu() {
        this(NOT_FOUND);
    }

    public void printMenu(){

        Console.println(menuCommandsList.stream()
                .map(MenuCommands::getDescription)
                .collect(Collectors.joining("\n",
                        "-----------------------------------------------------\n",
                        "\n-----------------------------------------------------"))
        );
    }
    public boolean isNotValidIndex(String stringLine) {
        boolean isValid = false;
        try {
            int indexMenu = Integer.parseInt(stringLine);
            isValid = (indexMenu > 0 && indexMenu <= this.menuCommandsList.size());
        }catch (NumberFormatException ex){

        }
        return !isValid;
    }

    public MenuCommands getMenuCommandsByIndex(int index){
        return menuCommandsList.get(index-1);
    }
}
