package homework11.menu;

import libs.ConsoleColor;

public enum MenuCommands {
    NOT_FOUND(),
    CREATE_TEST_DATA(NOT_FOUND,"1. Заполнить тестовыми данными."),
    SHOW_ALL_FAMILY_LIST(NOT_FOUND,"2. Отобразить весь список семей"),
    SHOW_FAMILIES_BIGGER_THAN(NOT_FOUND,"3. Отобразить список семей, где количество людей больше заданного"),
    SHOW_FAMILIES_LESS_THAN(NOT_FOUND,"4. Отобразить список семей, где количество людей меньше заданного"),
    COUNT_FAMILIES_WITH_MEMBER_NUMBER(NOT_FOUND,"5. Подсчитать количество семей, где количество членов равно"),
    CREATE_NEW_FAMILY(NOT_FOUND,"6. Создать новую семью"),
    DELETE_FAMILY_BY_INDEX(NOT_FOUND,"7. Удалить семью по индексу семьи в общем списке"),
    EDIT_FAMILY_BY_INDEX(NOT_FOUND,"8. Редактировать семью по индексу семьи в общем списке"),
    DELETE_ALL_CHILDREN_OLDER_THEN(NOT_FOUND,"9. Удалить всех детей старше возраста"),
    BORN_CHILD(EDIT_FAMILY_BY_INDEX, "1. Родить ребенка"),
    ADOPT_CHILD(EDIT_FAMILY_BY_INDEX, "2. Усыновить ребенка"),
    SAVE_IN_FILE(NOT_FOUND,"10. Сохранять данные локально на компьютер."),
    LOAD_FROM_FILE(NOT_FOUND,"11. Загрузить данные из файла."),
    EXIT(NOT_FOUND, ""+ConsoleColor.BLACK_BOLD+ConsoleColor.CYAN_BACKGROUND+"12. Выход"+ConsoleColor.RESET);

    private  String description;
    private MenuCommands parentMenu;

    private MenuCommands(String description){
        this.description = description;
    }
    private MenuCommands(MenuCommands parentMenu, String description){
        this.description = description;
        this.parentMenu = parentMenu;
    }

    private MenuCommands() {

    }

    public  String getDescription(){
        return this.description;
    }
    public MenuCommands getParentMenu(){
        return this.parentMenu;
    }
}
