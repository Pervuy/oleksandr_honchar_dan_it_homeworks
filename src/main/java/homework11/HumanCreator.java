package homework11;

public interface HumanCreator {

    Human bornChild(String nameMan, String nameWoman);
}
