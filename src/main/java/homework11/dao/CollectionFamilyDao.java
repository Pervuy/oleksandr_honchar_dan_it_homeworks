package homework11.dao;

import homework11.Family;

import java.util.ArrayList;
import java.util.List;

//Создайте класс CollectionFamilyDao: класс должен содержать List<Family>,
// в котором будут храниться все семьи (это будет наша база данных) и реализуйте в нем интерфейс FamilyDao.
public final class CollectionFamilyDao implements FamilyDao {
    private final List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int id) {
        return  (isIdValid(id)) ? families.get(id) : null;
    }

    @Override
    public boolean deleteFamily(int id) {

        return families.removeIf(n -> families.size()>id && n.equals(families.get(id)));
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        int i = families.indexOf(family);
        if (i>=0){
           families.add(i, family);
       }else {
           families.add(family);
       }
    }
    @Override
    public boolean loadData(List<Family> families) {
        return this.families.addAll(families);
    }

    public int count(){
        return families.size();
    }

    private boolean isIdValid(int id){
        return (id >= 0 &&  id < families.size() );
    }
}
