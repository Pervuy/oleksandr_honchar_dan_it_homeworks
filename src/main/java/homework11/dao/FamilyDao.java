package homework11.dao;

import homework11.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int id);
    boolean deleteFamily(int id);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
    boolean loadData(List<Family> families);

}
