package homework2;

import libs.ScannerConstrained;

public class UserAction {
    public static int getNumberRowOrColumn(String nameColRow) {

        int readNumber;

        System.out.printf("Enter number for %s (between 1 and 5)\n", nameColRow);
        ScannerConstrained scannerConstrained = new ScannerConstrained();
        String readStringLine = scannerConstrained.nextLine();

        try {
            readNumber = Integer.parseInt(readStringLine);

            if (readNumber > 5 || readNumber < 1) {
                readNumber = getNumberRowOrColumn(nameColRow);
            }
        } catch (NumberFormatException ex) {

            readNumber = getNumberRowOrColumn(nameColRow);
        }

        return readNumber;
    }

    public static void fire(Fild fild, int row, int column){
       fild.setCelling(row,column);
    }

}
