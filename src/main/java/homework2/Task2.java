package homework2;

import libs.Console;

public class Task2 {
    public static void main(String[] args) {

        Fild fild = new Fild();
        fild.print();

       Console.println("All set. Get ready to rumble!");

        while (!fild.isHitAllTarget()) {

            int row = UserAction.getNumberRowOrColumn("Row");
            int column = UserAction.getNumberRowOrColumn(("Column"));

            UserAction.fire(fild, row, column);
            fild.print();

        }
        Console.println("You have won!");

    }

}
