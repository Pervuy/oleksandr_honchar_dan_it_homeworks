package homework2;

import libs.Console;

import java.util.Random;

public class Fild {
    private final String[][] field;
    private final String[][] fieldWithTarget;

    public Fild(int hight, int width){

        field = new String[hight+1][width+1];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (i==0) {
                    field[i][j] = String.valueOf(j);
                } else if (j==0) {
                    field[i][j] = String.valueOf(i);
                } else {
                    field[i][j] = "-";
                }
            }
        }

        fieldWithTarget = new String[hight+1][width+1];
        for (int i = 0; i < fieldWithTarget.length; i++) {
            for (int j = 0; j < fieldWithTarget[i].length; j++) {
                if (i==0) {
                    fieldWithTarget[i][j] = String.valueOf(j);
                } else if (j==0) {
                    fieldWithTarget[i][j] = String.valueOf(i);
                } else {
                    fieldWithTarget[i][j] = "-";
                }
            }
        }
        Random random = new Random();
        int isHorizontal = random.nextInt(0,2);
        int row = random.nextInt(1,5);
        int column = random.nextInt(1,5);

        //System.out.printf("isHorizontal = %d; row = %d; column = %d\n",isHorizontal,row,column);

        if (isHorizontal==1){
            fieldWithTarget[row][column] = "O";
            for (int i = 1; i < 3; i++) {

                if (column < fieldWithTarget[row].length-1){
                    column++;
                    fieldWithTarget[row][column] = "O";
                } else {
                    fieldWithTarget[row][column - i] = "O";
                }
            }
        }else {
            fieldWithTarget[row][column] = "O";
            for (int i = 1; i < 3; i++) {

                if (row < fieldWithTarget.length-1){
                    row++;
                    fieldWithTarget[row][column] = "O";
                } else {
                    fieldWithTarget[row-i][column] = "O";
                }
            }
        }

    }

    public Fild(){
        this(5,5);
    }

    public void print() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String[] strings : field) {
            for (String string : strings) {
                stringBuilder.append(string).append("|");
            }
            stringBuilder.append("\n");
        }
        Console.println(stringBuilder.toString());
    }

    public void setCelling(int row, int column){
        String s = fieldWithTarget[row][column];
        if (s.equals("O")){
            fieldWithTarget[row][column] = "X";
            field[row][column] = "X";
        }else {
            field[row][column] = "*";
        }
    }

    public boolean isHitAllTarget(){

            for (int i = 0; i < fieldWithTarget.length; i++) {
                for (int j = 0; j < fieldWithTarget[i].length; j++) {
                    try {
                        if (fieldWithTarget[i][j].equals("O")) {
                            return false;
                        }
                    } catch (NullPointerException ex) {
                        Console.println( " i "+i+" j " + j);
                    }
                }
            }

        return true;
    }
}
