package homework09;

import libs.Console;

import java.util.Set;

public class Dog extends Pet implements Fouling{

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog() {
    }

    @Override
    public void foul(){
        Console.println("Нужно хорошо замести следы...");
    }

    @Override
    public void respond() {
        System.out.printf("ГАВ ГАВ, хозяин. Я - %s. Я соскучился!\n", super.getNickname());
    }
}
