package homework09;

import libs.Console;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, String> schedule = new TreeMap<>();
    private Family family;

    public Human(String name, String surname, String birthDay, int iq, Map<DayOfWeek, String> schedule) {
        this(name, surname, birthDay, iq);
        this.schedule = schedule;
    }
    public Human(String name, String surname, String birthDay, int iq){
        this(name,surname,birthDay);
        setIq(iq);
    }
    public Human(String name, String surname, String birthDay) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDay);
    }
    public Human() {
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return surname;
    }

    public void setBirthDate(String birthDay) {

        String birthDayPattern = "\\d{2}/\\d{2}/\\d{4}";

        if (!birthDay.matches(birthDayPattern)) {
            throw new IllegalArgumentException("Дата рождения должна быть задана в формате 20/03/2016");
        }

        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder().appendPattern("dd/MM/yyyy").toFormatter();
        this.birthDate = LocalDate.parse(birthDay, dateTimeFormatter)
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC)
                .getEpochSecond();
    }
    public String getBirthDate(){
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(this.birthDate), ZoneOffset.UTC)
                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public void setIq(int iq) {
        if (iq < 0 || iq > 100) {
            throw new IllegalArgumentException("Уровень IQ должен быть в диапазоне от 0 до 100.");
        }
        this.iq = iq;
    }
    public int getIq() {
        return iq;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }
    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    public Family getFamily(){
        return family;
    }

    public String getFullName(){
        return String.format("%s %s",name, surname);
    }
    public String describeAge(){

        Period period = Period.between(LocalDate.ofInstant(Instant.ofEpochSecond(this.birthDate), ZoneOffset.UTC),LocalDate.now());
        long years = period.getYears();
        long months = period.getMonths();
        long days = period.getDays();
        return String.format("%d years, %d months, %d days%n",years,months,days);
    }
    @Override
    public boolean equals(Object anObject) {
        if (this == anObject) return true;
        if (anObject == null || getClass() != anObject.getClass()) return false;
        if (this.hashCode() == anObject.hashCode())  return true;
        Human human = (Human) anObject;

        return birthDate == human.birthDate && name.equals(human.name) && surname.equals(human.surname);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }
    @Override
    public String toString() {

        //Human{name='Name', surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
        return String.format("Human{name=%s, surname=%s, birthday=%s, iq=%d, schedule=%s}",
                name,
                surname,
                this.getBirthDate(),
                iq,
                schedule.toString());
    }

    public void greetPet(Pet pet){
        if (pet != null){
            System.out.printf("Привет, %s\n",pet.getNickname());
        }
    }
    public void describePet(){
        if (family != null && family.getPets() != null){
            family.getPets()
                    .forEach(pet -> System.out.printf("У меня есть %s, ему %d лет, он %s\n", pet.getSpecies(), pet.getAge(), pet.getDescribeTrick()));

        }else {
            Console.println("У мене нету питомца");
        }
    }
    public boolean feedPet(boolean isTimeFeed, Pet pet, int trickLevelRandom){

        if (pet == null) return false;

        boolean petIsFeed = false;

        String message;
        if (isTimeFeed || pet.getTrickLevel() > trickLevelRandom) {
            message = String.format("Хм... покормлю ка я %s%n", pet.getNickname());
            petIsFeed = true;
        } else {
            message = String.format("Думаю, %s не голоден.%n", pet.getNickname());
        }

        Console.println(message);
        return petIsFeed;
    }

    public Human getFather(){
        if(family==null) return null;
        return family.getFather();
    }
    public Human getMother(){
        if(family==null) return null;
        return family.getMother();
    }

    @Override
    protected void finalize()  {
        System.out.printf("Finalize object %s%n",this.toString());
    }

}
