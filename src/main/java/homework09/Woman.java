package homework09;

import libs.Console;

import java.util.Map;

public final class Woman extends Human{

    public Woman(String name, String surname, String birthDay, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDay, iq, schedule);
    }

    public Woman(String name, String surname, String birthDay) {
        super(name, surname, birthDay);
    }

    public Woman(String name, String surname, String birthDay, int iq){
        super(name, surname, birthDay, iq);
    }
    public Woman() {
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.printf("Hello, my dear %s\n", pet.getNickname());

    }
    @Override
    public String toString() {
        return String.format("Woman{name=%s, surname=%s, birthday=%s, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getBirthDate(),
                super.getIq(),
                super.getSchedule().toString());
    }

    public void makeup(){
        Console.println("I like makeup!");
    }
}
