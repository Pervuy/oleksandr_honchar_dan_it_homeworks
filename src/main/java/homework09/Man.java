package homework09;

import libs.Console;

import java.util.Map;

public final class Man extends Human{
    public Man(String name, String surname, String birthDay, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDay, iq, schedule);
    }

    public Man(String name, String surname, String birthDay) {
        super(name, surname, birthDay);
    }

    public Man(String name, String surname, String birthDay, int iq){
        super(name, surname, birthDay, iq);
    }
    public Man() {
    }

   @Override
    public void greetPet(Pet pet) {
       System.out.printf("Hi, %s\n", pet.getNickname());
    }

    @Override
    public String toString() {
        return String.format("Man{name=%s, surname=%s, birthday=%s, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getBirthDate(),
                super.getIq(),
                super.getSchedule().toString());
    }

    public void repairCar(){
        Console.println("I like repair car");
    }
}
