package homework09;

import libs.Console;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet implements Comparable<Pet> {

    private final Species species = Species.getValue(this);
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits = new HashSet<>();

    public Pet( String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet() {
    }

    public void eat(){
        Console.println("I eat!\n");
    }
    public abstract void respond();

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public String getDescribeTrick() {
        return (this.trickLevel <= 50?("почти не хитрый"):("очень хитрый"));
    }

    public Set<String> getHabits() {
        return habits;
    }

    @Override
    public String toString() {

        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=%s, canFly=%b, hasFur=%b, numberOfLegs=%d}",
                species,
                nickname,
                age,
                trickLevel,
                habits.toString(),
                species.isCanFly(),
                species.isHasFur(),
                species.getNumberOfLegs());

    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel < 0 || trickLevel > 100) {
            throw new IllegalArgumentException("Уровень хитрости должен быть в диапазоне от 0 до 100.");
        }
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }


    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (this.hashCode() == o.hashCode())  return true;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && species == pet.species && Objects.equals(nickname, pet.nickname);
    }
    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }


    @Override
    public int compareTo(Pet pet) {
        return this.species.compareTo(pet.species);
    }
}
