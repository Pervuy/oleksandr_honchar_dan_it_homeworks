package homework06;

import libs.Console;

public class Main {
    public static void main(String[] args) {
        Human father1 = new Man("Paul", "Volcov", 1980);
        father1.setIq(90);
        Human mother1 = new Woman("Lara", "Kroft", 1990);
        mother1.setIq(70);
        Family family1 = new Family(mother1, father1);
        /*Human child1 = new Human("Sasha", "First", 2000);
        Human child2 = new Human("Sasha2", "Second", 2002);
        family1.addChild(child1);
        family1.addChild(child2);

        Pet pet1 = new Dog("Kesha", 1, 99, new String[]{"Sleep", "Eat"});
        family1.setPet(pet1);

        father1.greetPet();
        mother1.greetPet();*/

        for (int i = 0; i < 20; i++) {
            Human child1 = family1.bornChild();
            Console.println(child1.toString());
        }
        Console.println(family1.toString());
    }
}
