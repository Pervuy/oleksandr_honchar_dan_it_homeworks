package homework06;

import libs.Console;

import java.util.Arrays;

public final class Man extends Human{
    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        if (super.getFamily() != null && super.getFamily().getPet() != null){
            System.out.printf("Hi, %s\n",super.getFamily().getPet().getNickname());
        }
    }

    @Override
    public String toString() {
        return String.format("Man{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getYear(),
                super.getIq(),
                Arrays.deepToString(super.getSchedule()));
    }

    public void repairCar(){
        Console.println("I like repair car");
    }
}
