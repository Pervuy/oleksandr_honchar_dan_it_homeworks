package homework06;

public enum DayOfWeek {
    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    NOT_FOUND("Not found");

    private final String value;
    private DayOfWeek(final String value){
        this.value = value;
    }

    public static String getValue(DayOfWeek dayOfWeek){
        return dayOfWeek.value;
    }
}
