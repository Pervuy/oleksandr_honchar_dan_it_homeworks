package homework06;

import libs.Console;

public class DomesticCat extends Pet implements Fouling{


    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat() {
    }

    @Override
    public void respond() {
        System.out.printf("НЯВ НЯВ, хозяин. Я - %s. Я соскучился!\n", super.getNickname());
    }

    @Override
    public void foul(){
        Console.println("Нужно хорошо замести следы...");
    }
}
