package homework06;

import libs.Console;

import java.util.Arrays;

public final class Woman extends Human{

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman() {
    }

    @Override
    public void greetPet() {
        if (super.getFamily() != null && super.getFamily().getPet() != null){
            System.out.printf("Hello, my dear %s\n",super.getFamily().getPet().getNickname());
        }
    }
    @Override
    public String toString() {
        return String.format("Woman{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}",
                super.getName(),
                super.getSurname(),
                super.getYear(),
                super.getIq(),
                Arrays.deepToString(super.getSchedule()));
    }

    public void makeup(){
        Console.println("I like makeup!");
    }
}
