package homework4;

import libs.Console;

import java.util.Objects;

public class Main {
    public static void main(String[] args) {

        Human father1 = new Human("Paul","Volcov",1980);
        Human mother1 = new Human("Lara","Kroft",1990);
        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        Human child3 = new Human("Sasha3","Third",2004);
        Pet pet1 = new Pet("Dog","Roki",5,30,new String[0]);

        Family family1 = new Family(mother1,father1);
        family1.setPet(pet1);

        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);

        family1.deleteChild(child2);
        Console.println(family1.toString());
        System.out.println();
        Human mother = child1.getMother();
        Human father = child2.getFather();
        Console.println(mother.toString());
        Console.println(Objects.toString(father));

        System.out.println();

        father1.greetPet();
        mother1.greetPet();
        child1.greetPet();
        child2.greetPet();
        child3.greetPet();

        System.out.println();

        father1.describePet();
        mother1.describePet();
        child1.describePet();
        child2.describePet();
        child3.describePet();

        System.out.println();
        father1.feedPet(true);
        mother1.feedPet(false);
        child1.feedPet(false);
        //child2.feedPet(false);
        child3.feedPet(false);

    }
}
