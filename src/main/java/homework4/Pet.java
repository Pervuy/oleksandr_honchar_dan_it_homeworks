package homework4;

import libs.Console;

import java.util.Arrays;

public class Pet {

    static {
        String className = Thread.currentThread()
                .getStackTrace()[1]
                .getClassName();
        System.out.printf("Loading class %s%n",className);
    }
    {
        System.out.printf("Create object %s%n",this.getClass().getCanonicalName());
    }

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        setTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public void eat(){
        Console.println("Я кушаю!");
    }
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }
    public void foul(){
        Console.println("Нужно хорошо замести следы...");
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public String getDescribeTrick() {
        return (this.trickLevel <= 50?("почти не хитрый"):("очень хитрый"));
    }

    public String[] getHabits() {
        return habits;
    }

    @Override
    public String toString() {
       // dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=%s\n}",
                species,
                nickname,
                age,
                trickLevel,
                Arrays.toString(habits));

    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel < 0 || trickLevel > 100) {
            throw new IllegalArgumentException("Уровень хитрости должен быть в диапазоне от 0 до 100.");
        }
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
