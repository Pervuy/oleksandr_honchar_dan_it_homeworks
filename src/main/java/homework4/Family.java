package homework4;

import java.util.Arrays;

public class Family {

    static {
        String className = Thread.currentThread()
                .getStackTrace()[1]
                .getClassName();
        System.out.printf("Loading class %s%n",className);
    }
    {
        System.out.printf("Create object %s%n",this.getClass().getCanonicalName());
    }

    private final Human mother;
    private final Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        //единственным условием создания новой семьи является наличие 2-х родителей,
        // при этом у родителей должна устанавливаться ссылка на текущую новую семью, а семья создается с пустым массивом детей.
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
        this.children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public void addChild(Human  child){

        child.setFamily(this);
        children = Arrays.copyOf(children,children.length+1);
        children[children.length-1] = child;

    }

    public int countFamily(){
        int countParent = 2;
        int countChildren = children.length;
        return countParent + countChildren;
    }

    public boolean deleteChild(int indexChild){

        if (indexChild < 0) throw new IllegalStateException("index must be positive");
        if (indexChild > children.length-1) throw new IllegalStateException("index must be less than length");

        Human child = children[indexChild];
        child.setFamily(null);

        Human[] newChildren= new Human[children.length - 1];

        System.arraycopy(children, 0, newChildren, 0, indexChild);
        System.arraycopy(children, indexChild + 1, newChildren, indexChild, children.length - indexChild - 1);

        children = newChildren;

        return true;
    }

    public boolean deleteChild(Human child){
        Human childFromFamily;
        for (int i = 0; i < children.length; i++) {
            childFromFamily = children[i];
            if (childFromFamily.equals(child)) return deleteChild(i);
        }
        return false;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return String.format("Family{\n mother=%S,\n father=%S,\n children=%S,\n  pet=%S }",
                mother.toString(),
                father.toString(),
                Arrays.toString(children),
                (pet==null) ?("null"):(pet.toString()));
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }
}
