package homework09;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    Pet dog;
    Pet dog2;
    Pet dog3;

    Pet fish;
    Pet fish2;
    Pet fish3;
    Pet cat;
    Pet cat2;
    Pet cat3;

    Pet pet;

    @BeforeEach
    void setupThis(){

        dog = new Dog("Rokki",5,30, Set.of("Sleep, Eat, Bark"));
        dog2 = new Dog("Rokki");
        dog3 = new Dog();

        fish = new Fish("Gold fish",30,0, Set.of("Float, Eat"));
        fish2 = new Fish("Gold fish");
        fish3 = new Fish();

        cat = new DomesticCat("Gavrykha",2,60, Set.of("Run, Eat, Bite"));
        cat2 = new DomesticCat("Gavrykha");
        cat3 = new DomesticCat();
        pet = new Pet() {
            @Override
            public void respond() {

            }
        };
    }
    @AfterEach
    void afterThis(){
        dog = null;
        dog2 = null;
        dog3 = null;

        fish = null;
        fish2 = null;
        fish3 = null;

        cat = null;
        cat2 = null;
        cat3 = null;

        pet = null;
    }

    @Test
    void testGetSpecies() {

        assertEquals(Species.DOG, dog.getSpecies());
        assertEquals(Species.DOG, dog2.getSpecies());
        assertEquals(Species.DOG, dog3.getSpecies());

        assertEquals(Species.FISH, fish.getSpecies());
        assertEquals(Species.FISH, fish2.getSpecies());
        assertEquals(Species.FISH, fish3.getSpecies());

        assertEquals(Species.DOMESTIC_CAT, cat.getSpecies());
        assertEquals(Species.DOMESTIC_CAT, cat2.getSpecies());
        assertEquals(Species.DOMESTIC_CAT, cat3.getSpecies());

        assertEquals(Species.UNKNOWN, pet.getSpecies());

    }

    @Test
    void testGetNickname() {

        assertEquals("Rokki", dog.getNickname());
        assertEquals("Rokki", dog2.getNickname());
        assertNull(dog3.getNickname());
        dog3.setNickname("Rokki");
        assertEquals("Rokki", dog3.getNickname());

        assertEquals("Gold fish", fish.getNickname());
        assertEquals("Gold fish", fish2.getNickname());
        assertNull(fish3.getNickname());
        fish3.setNickname("Gold fish");
        assertEquals("Gold fish", fish3.getNickname());

        assertEquals("Gavrykha", cat.getNickname());
        assertEquals("Gavrykha", cat2.getNickname());
        assertNull(cat3.getNickname());
        cat3.setNickname("Gavrykha");
        assertEquals("Gavrykha", cat3.getNickname());

        assertNull(pet.getNickname());
    }

    @Test
    void testGetAge() {
        assertEquals(5, dog.getAge());
        assertEquals(0, dog2.getAge());
        assertEquals(0, dog3.getAge());
        dog3.setAge(6);
        assertEquals(6, dog3.getAge());

        assertEquals(30, fish.getAge());
        assertEquals(0, fish2.getAge());
        assertEquals(0, fish3.getAge());
        fish3.setAge(20);
        assertEquals(20, fish3.getAge());

        assertEquals(2, cat.getAge());
        assertEquals(0, cat2.getAge());
        assertEquals(0, cat3.getAge());
        cat3.setAge(1);
        assertEquals(1, cat3.getAge());

        assertEquals(0, pet.getAge());
    }

    @Test
    void testGetDescribeTrick() {
        assertEquals("почти не хитрый", dog.getDescribeTrick());
        assertEquals("почти не хитрый", dog2.getDescribeTrick());
        assertEquals("почти не хитрый", dog3.getDescribeTrick());
        dog3.setTrickLevel(51);
        assertEquals("очень хитрый", dog3.getDescribeTrick());

        assertEquals("почти не хитрый", fish.getDescribeTrick());
        assertEquals("почти не хитрый", fish2.getDescribeTrick());
        assertEquals("почти не хитрый", fish3.getDescribeTrick());
        fish3.setTrickLevel(100);
        assertEquals("очень хитрый", fish3.getDescribeTrick());

        assertEquals("очень хитрый", cat.getDescribeTrick());
        assertEquals("почти не хитрый", cat2.getDescribeTrick());
        assertEquals("почти не хитрый", cat3.getDescribeTrick());
        cat3.setTrickLevel(1);
        assertEquals("почти не хитрый", cat3.getDescribeTrick());

        assertEquals("почти не хитрый", pet.getDescribeTrick());
    }

    @Test
    void testGetHabits() {
        assertEquals(Set.of("Sleep, Eat, Bark"), dog.getHabits());
        assertEquals(new HashSet<>(), dog2.getHabits());
        assertEquals(new HashSet<>(), dog3.getHabits());
        dog3.setHabits(Set.of("Sleep, Eat"));
        assertEquals(Set.of("Sleep, Eat"), dog3.getHabits());

        assertEquals(Set.of("Float, Eat"), fish.getHabits());
        assertEquals(new HashSet<>(), fish2.getHabits());
        assertEquals(new HashSet<>(), fish3.getHabits());
        fish3.setHabits(Set.of("Float"));
        assertEquals(Set.of("Float"), fish3.getHabits());

        assertEquals(Set.of("Run, Eat, Bite"), cat.getHabits());
        assertEquals(new HashSet<>(), cat2.getHabits());
        assertEquals(new HashSet<>(), cat3.getHabits());
        cat3.setHabits(Set.of("Run"));
        assertEquals(Set.of("Run"), cat3.getHabits());

        assertEquals(new HashSet<>(), pet.getHabits());
    }

    @Test
    void testToString() {
        assertEquals("DOG{nickname=Rokki, age=5, trickLevel=30, habits=[Sleep, Eat, Bark], canFly=false, hasFur=true, numberOfLegs=4}",  dog.toString());
        assertEquals("DOG{nickname=Rokki, age=0, trickLevel=0, habits=[], canFly=false, hasFur=true, numberOfLegs=4}",  dog2.toString());
        assertEquals("DOG{nickname=null, age=0, trickLevel=0, habits=[], canFly=false, hasFur=true, numberOfLegs=4}",  dog3.toString());

        assertEquals("FISH{nickname=Gold fish, age=30, trickLevel=0, habits=[Float, Eat], canFly=false, hasFur=false, numberOfLegs=0}",  fish.toString());

        assertEquals("DOMESTIC_CAT{nickname=Gavrykha, age=2, trickLevel=60, habits=[Run, Eat, Bite], canFly=false, hasFur=true, numberOfLegs=4}",  cat.toString());

        assertEquals("UNKNOWN{nickname=null, age=0, trickLevel=0, habits=[], canFly=false, hasFur=false, numberOfLegs=0}",  pet.toString());
    }

    @Test
    void getTrickLevel() {
        assertEquals(30, dog.getTrickLevel());
        assertEquals(0, dog2.getTrickLevel());
        assertEquals(0, dog3.getTrickLevel());
        dog3.setTrickLevel(60);
        assertEquals(60, dog3.getTrickLevel());

        assertEquals(0, fish.getTrickLevel());
        assertEquals(0, fish2.getTrickLevel());
        assertEquals(0, fish3.getTrickLevel());
        fish3.setTrickLevel(20);
        assertEquals(20, fish3.getTrickLevel());

        assertEquals(60, cat.getTrickLevel());
        assertEquals(0, cat2.getTrickLevel());
        assertEquals(0, cat3.getTrickLevel());
        cat3.setTrickLevel(1);
        assertEquals(1, cat3.getTrickLevel());

        assertEquals(0, pet.getTrickLevel());
    }

    @Test
    public void testEquals_Symmetric(){
        Pet dog4 = new Dog();

        assertEquals(dog3.hashCode(), dog4.hashCode());
        assertTrue(dog3.equals(dog4) && dog4.equals(dog3));

    }
    @Test
    public void testEquals_Symmetric2(){
        Pet dog4 = new Dog("Rokki",5,30, Set.of("Sleep, Eat, Bark"));

        assertEquals(dog.hashCode(), dog4.hashCode());
        assertTrue(dog.equals(dog4) && dog4.equals(dog));
    }
    @Test
    public void testEquals_NotSymmetric1(){
        Pet dog4 = new Dog("Rokki3",15,60, Set.of("Sleep, Eat, Bark"));

        assertTrue(dog.hashCode() != dog4.hashCode());
        assertTrue(!dog.equals(dog4) && !dog4.equals(dog));
    }
    @Test
    public void testEquals_NotSymmetric2(){
        Pet cat4 = new DomesticCat("Rokki",5,30, Set.of("Sleep, Eat, Bark"));

        assertTrue(dog.hashCode() != cat4.hashCode());
        assertTrue(!dog.equals(cat4) && !cat4.equals(dog));
    }

}