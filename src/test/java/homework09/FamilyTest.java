package homework09;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    Human man;
    Human man2;
    Human woman;
    Human woman2;
    Family family;
    Family family2;

    @BeforeEach
    void setupThis() {
        // использую TreeMap, чтоб добится четкого порядка для вывода schedule в toString,
        // так как просто Map этого не гарантирует
        man = new Man("Paul", "Volcov", "01/01/1980", 90, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        man2 = new Man("Paul", "Fox", "04/06/1950");

        woman = new Woman("Lara", "Kroft", "22/05/1990", 75, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        woman2 = new Woman("Svetlana", "Fox", "31/12/1960");

        family = new Family(woman, man);
        family2 = new Family(woman2, man2);

        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Pet fish = new Fish("Gold fish", 30, 0, Set.of("Float, Eat"));
        Pet cat = new DomesticCat("Gavrykha", 2, 60, Set.of("Run, Eat, Bite"));
        Set<Pet> pets = new TreeSet<>(Set.of(dog,fish,cat));
        family.setPet(pets);
    }
    @AfterEach
    void afterThis(){
        man = null;
        man2 = null;
        woman = null;
        woman2 = null;
        family = null;
        family2 = null;
    }

    @Test
    void testGetMother() {
        assertEquals(woman, family.getMother());
        assertEquals(woman2, family2.getMother());
    }

    @Test
    void testGetFather() {
        assertEquals(man, family.getFather());
        assertEquals(man2, family2.getFather());
    }

    @Test
    void testAddChild() {

        Human child = new Man("Sasha","Volcov","25/05/1999");
        assertTrue(family.addChild(child));
        assertFalse(family.addChild(child));
    }

    @Test
    void testCountFamily() {

        assertEquals(2,family.countFamily());
        Human child = new Man("Sasha","Volcov","05/05/1999");
        Human child2 = new Woman("Lena","Volcov","16/10/2001");
        family.addChild(child);
        assertEquals(3,family.countFamily());
        family.addChild(child2);
        assertEquals(4,family.countFamily());
    }

    @Test
    void testGetChild() {
        Human child = new Man("Sasha","Volcov","05/05/1999");
        Human child2 = new Woman("Lena","Volcov","05/07/2001");
        assertNull(family.getChild(0));
        family.addChild(child);
        assertEquals(child, family.getChild(0));
        family.addChild(child2);
        assertEquals(child2, family.getChild(1));
    }

    @Test
    void getIndexChild() {
        Human child = new Man("Sasha","Volcov","05/05/1999");
        Human child2 = new Woman("Lena","Volcov","05/07/2001");
        assertEquals(-1, family.getIndexChild(child));
        family.addChild(child);
        assertEquals(0, family.getIndexChild(child));
        family.addChild(child2);
        assertEquals(1, family.getIndexChild(child2));
    }

    @Test
    void testDeleteChild() {
        Human child = new Man("Sasha","Volcov","05/05/1999");
        Human child2 = new Woman("Lena","Volcov","05/07/2001");
        assertFalse(family.deleteChild(child));
        family.addChild(child);
        family.addChild(child2);
        assertTrue(family.deleteChild(child));
        assertFalse(family.deleteChild(1));
        assertTrue(family.deleteChild(0));
    }

    @Test
    void testGetPets() {
        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Pet fish = new Fish("Gold fish", 30, 0, Set.of("Float, Eat"));
        Pet cat = new DomesticCat("Gavrykha", 2, 60, Set.of("Run, Eat, Bite"));
        Set<Pet> pets2 = Set.of(dog, fish, cat);
        assertEquals(pets2, family.getPets());
        assertEquals(new TreeSet<>(), family2.getPets());
    }

    @Test
    void getChildren() {
        Human child = new Man("Sasha","Volcov","05/05/1999");
        Human child2 = new Woman("Lena","Volcov","05/07/2001");
        List<Human> children = List.of(child,child2);
        family.addChild(child);
        family.addChild(child2);

        assertEquals(children, family.getChildren());
        assertEquals(new ArrayList<>(), family2.getChildren());
    }

    @Test
    void testToString() {

        assertEquals("Family{\n " +
                "mother=WOMAN{NAME=LARA, SURNAME=KROFT, BIRTHDAY=22/05/1990, IQ=75, SCHEDULE={MONDAY=WALK, FRIDAY=SLEEP}},\n " +
                "father=MAN{NAME=PAUL, SURNAME=VOLCOV, BIRTHDAY=01/01/1980, IQ=90, SCHEDULE={MONDAY=WALK, FRIDAY=SLEEP}},\n " +
                "children=[],\n  " +
                "pet=[DOG{NICKNAME=ROKKI, AGE=5, TRICKLEVEL=30, HABITS=[SLEEP, EAT, BARK], CANFLY=FALSE, HASFUR=TRUE, NUMBEROFLEGS=4}, FISH{NICKNAME=GOLD FISH, AGE=30, TRICKLEVEL=0, HABITS=[FLOAT, EAT], CANFLY=FALSE, HASFUR=FALSE, NUMBEROFLEGS=0}, DOMESTIC_CAT{NICKNAME=GAVRYKHA, AGE=2, TRICKLEVEL=60, HABITS=[RUN, EAT, BITE], CANFLY=FALSE, HASFUR=TRUE, NUMBEROFLEGS=4}]}",  family.toString());

        assertEquals("Family{\n " +
                "mother=WOMAN{NAME=SVETLANA, SURNAME=FOX, BIRTHDAY=31/12/1960, IQ=0, SCHEDULE={}},\n " +
                "father=MAN{NAME=PAUL, SURNAME=FOX, BIRTHDAY=04/06/1950, IQ=0, SCHEDULE={}},\n " +
                "children=[],\n  " +
                "pet=[]}",  family2.toString());
    }

    @Test
    public void testEquals_Symmetric(){
        Human man3 = new Man("Paul", "Fox", "04/06/1950");
        Human woman3 = new Woman("Svetlana", "Fox", "31/12/1960");

        Family family3 = new Family(woman3, man3);

        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Pet fish = new Fish("Gold fish", 30, 0, Set.of("Float, Eat"));
        Pet cat = new DomesticCat("Gavrykha", 2, 60, Set.of("Run, Eat, Bite"));
        Set<Pet> pets = new TreeSet<>(Set.of(dog,fish,cat));
        family3.setPet(pets);

        assertEquals(family2.hashCode(), family3.hashCode());
        assertTrue(family2.equals(family3) && family3.equals(family2));

    }

    @Test
    public void testEquals_NotSymmetric1(){

        assertTrue(family.hashCode() != family2.hashCode());
        assertTrue(!family.equals(family2) && !family2.equals(family));
    }


}