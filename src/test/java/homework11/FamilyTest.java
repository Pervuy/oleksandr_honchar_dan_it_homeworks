package homework11;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Human man1;
    Human man2;
    Human woman1;
    Human woman2;
    Family family1;
    Family family2;

    @BeforeEach
    void setupThis() {
        // использую TreeMap, чтоб добится четкого порядка для вывода schedule в toString,
        // так как просто Map этого не гарантирует
        man1 = new Man("Paul", "Volcov", "01/01/1980", 90, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        man2 = new Man("Paul", "Fox", "04/06/1950");

        woman1 = new Woman("Lara", "Kroft", "22/05/1990", 75, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        woman2 = new Woman("Svetlana", "Fox", "31/12/1960");

        family1 = new Family(woman1, man1);
        family2 = new Family(woman2, man2);

        Human child1 = new Man("Sasha", "Volcov", "05/05/2015");
        Human child2 = new Woman("Lena", "Volcov", "05/07/2021");
        family1.addChild(child1);
        family1.addChild(child2);

        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Set<Pet> pets = new TreeSet<>(Set.of(dog));
        family1.setPets(pets);
    }
    @AfterEach
    void afterThis(){
        man1 = null;
        man2 = null;
        woman1 = null;
        woman2 = null;
        family1 = null;
        family2 = null;
    }

    @Test
    void testPrettyFormat() {
        String expected = """
                family:
                         mother: {name='Lara', surname='Kroft', birthday='22/05/1990', iq=75, schedule={MONDAY=Walk, FRIDAY=Sleep}},
                         father: {name='Paul', surname='Volcov', birthday='01/01/1980', iq=90, schedule={MONDAY=Walk, FRIDAY=Sleep}},
                         children:
                                 boy: {name='Sasha', surname='Volcov', birthday='05/05/2015', iq=0, schedule={}}
                				girl: {name='Lena', surname='Volcov', birthday='05/07/2021', iq=0, schedule={}}
                         pets: [DOG{nickname=Rokki, age=5, trickLevel=30, habits=[Sleep, Eat, Bark], canFly=false, hasFur=true, numberOfLegs=4}]
                """;
        assertEquals(expected, family1.prettyFormat());
        System.out.println(family1.prettyFormat());
    }

    @Test
    void testGetChildrenPrettyFormat() {

        assertEquals("boy: {name='Sasha', surname='Volcov', birthday='05/05/2015', iq=0, schedule={}}\n\t\t\t\t" +
                "girl: {name='Lena', surname='Volcov', birthday='05/07/2021', iq=0, schedule={}}",family1.getChildrenPrettyFormat());
    }
}