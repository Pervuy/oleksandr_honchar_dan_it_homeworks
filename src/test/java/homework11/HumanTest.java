package homework11;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testPrettyFormat() {
        Human man = new Man("Paul", "Fox", "04/06/1950");
        Human woman = new Woman("Svetlana", "Fox", "31/12/1960");

        assertEquals("{name='Paul', surname='Fox', birthday='04/06/1950', iq=0, schedule={}}", man.prettyFormat());
        assertEquals("{name='Svetlana', surname='Fox', birthday='31/12/1960', iq=0, schedule={}}", woman.prettyFormat());
    }

    @Test
    void testPrettyChildFormat() {

        Human man = new Man("Paul", "Fox", "04/06/1950");
        Human woman = new Woman("Svetlana", "Fox", "31/12/1960");

        assertEquals("boy: {name='Paul', surname='Fox', birthday='04/06/1950', iq=0, schedule={}}", man.prettyChildFormat());
        assertEquals("girl: {name='Svetlana', surname='Fox', birthday='31/12/1960', iq=0, schedule={}}", woman.prettyChildFormat());
    }

    @Test
    void testGetGenderChild() {
        Human man = new Man("Paul", "Fox", "04/06/1950");
        Human woman = new Woman("Svetlana", "Fox", "31/12/1960");
        assertEquals("boy",man.getGenderChild());
        assertEquals("girl",woman.getGenderChild());
    }
}