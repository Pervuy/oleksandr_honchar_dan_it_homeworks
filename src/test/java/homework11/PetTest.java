package homework11;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void prettyFormat() {

        Pet dog = new Dog("Rokki",5,30, Set.of("Sleep, Eat, Bark"));

        assertEquals("DOG: {nickname='Rokki', age=5, trickLevel=30, habits=[Sleep, Eat, Bark]}",dog.prettyFormat());
        Pet dog2 = new Dog("Rokki");
        assertEquals("DOG: {nickname='Rokki', age=0, trickLevel=0, habits=[]}",dog2.prettyFormat());
    }
}