package homework05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {

    @Test
    public void testToString1(){
        // Тестируемый класс
        Human human = new Human();
        // Проверяемый метод
        assertEquals("Human{name=null, surname=null, year=0, iq=0, schedule=null}",  human.toString());

    }
    @Test
    public void testToString2(){
        // Тестируемый класс
        Human human = new Human("Paul","Volcov",1980);
        // Проверяемый метод
        assertEquals("Human{name=Paul, surname=Volcov, year=1980, iq=0, schedule=null}",  human.toString());

    }
    @Test
    public void testToString3(){
        // Тестируемый класс
        Human human = new Human("Paul","Volcov",1980,90,new String[][]{{"Friday","Sleep"},{"Saturday","Walk"}});

        // Проверяемый метод
        assertEquals("Human{name=Paul, surname=Volcov, year=1980, iq=90, schedule=[[Friday, Sleep], [Saturday, Walk]]}",  human.toString());

    }
    @Test
    public void testEquals_Symmetric(){
        Human human1 = new Human();
        Human human2 = new Human();

        assertEquals(human1.hashCode(), human2.hashCode());
        assertTrue(human1.equals(human2) && human2.equals(human1));

    }
    @Test
    public void testEquals_Symmetric2(){
        Human human1 = new Human("Paul","Volcov",1980);
        Human human2 = new Human("Paul","Volcov",1980);

        assertEquals(human1.hashCode(), human2.hashCode());
        assertTrue(human1.equals(human2) && human2.equals(human1));
    }
    @Test
    public void testEquals_NotSymmetric1(){
        Human human1 = new Human("Paul","Volcov",1980,90,new String[][]{{"Friday","Sleep"},{"Saturday","Walk"}});
        Human human2 = new Human("Paul","Volcov",1980);

        assertTrue(human1.hashCode() != human2.hashCode());
        assertTrue(!human1.equals(human2) && !human2.equals(human1));
    }


}
