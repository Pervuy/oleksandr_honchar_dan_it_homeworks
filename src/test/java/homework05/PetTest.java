package homework05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        // Тестируемый класс
        Pet pet = new Pet(Species.DOG,"Sharik",5,50,new String[]{"Sleep","Eat"});
        // Проверяемый метод
        assertEquals("DOG{nickname=Sharik, age=5, trickLevel=50, habits=[Sleep, Eat], canFly=false, hasFur=true, numberOfLegs=4}",  pet.toString());
    }
    @Test
    void testToString2() {
        // Тестируемый класс
        Pet pet = new Pet(Species.PARROT,"Kesha",1,99,new String[]{"Speak","Eat"});
        // Проверяемый метод
        assertEquals("PARROT{nickname=Kesha, age=1, trickLevel=99, habits=[Speak, Eat], canFly=true, hasFur=false, numberOfLegs=2}",  pet.toString());
    }
    @Test
    public void testEquals_Symmetric(){
        Pet pet1 = new Pet();
        Pet pet2 = new Pet();

        assertEquals(pet1.hashCode(), pet2.hashCode());
        assertTrue(pet1.equals(pet2) && pet2.equals(pet1));

    }
    @Test
    public void testEquals_Symmetric2(){
        Pet pet1 = new Pet(Species.PARROT,"Kesha",1,99,new String[]{"Speak","Eat"});
        Pet pet2 = new Pet(Species.PARROT,"Kesha",1,99,new String[]{"Speak","Eat"});

        assertEquals(pet1.hashCode(), pet2.hashCode());
        assertTrue(pet1.equals(pet2) && pet2.equals(pet1));
    }
    @Test
    public void testEquals_NotSymmetric1(){
        Pet pet1 = new Pet(Species.PARROT,"Kesha",1,99,new String[]{"Speak","Eat"});
        Pet pet2 = new Pet(Species.DOG,"Sharik",5,50,new String[]{"Sleep","Eat"});

        assertTrue(pet1.hashCode() != pet2.hashCode());
        assertTrue(!pet1.equals(pet2) && !pet2.equals(pet1));
    }

}