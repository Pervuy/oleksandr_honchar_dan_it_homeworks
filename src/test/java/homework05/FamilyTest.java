package homework05;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Family family1;

    @BeforeEach
    void setupThis(){
        Human father1 = new Human("Paul","Volcov",1980);
        Human mother1 = new Human("Lara","Kroft",1990);
        family1 = new Family(mother1, father1);
    }
    @AfterEach
    void afterThis(){
        family1 = null;
    }

    @Test
    void testDeleteChild() {
        //проверьте, что ребенок действительно удаляется из массива children (если передать объект, еквивалентый хотябы одному элементу массива);

        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        assertTrue(family1.addChild(child1));
        assertTrue(family1.addChild(child2));
        assertFalse(family1.addChild(child2));
        assertEquals(0,family1.getIndexChild(child1));
        assertEquals(1,family1.getIndexChild(child2));

        assertTrue(family1.deleteChild(child1));
    }
    @Test
    void testDeleteChild2() {
        //проверьте, что массив children остается без изменений (если передать объект, не еквивалентый ни одному элементу массива);

        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        Human child3 = new Human("Sasha2","Third",2003);
        assertTrue(family1.addChild(child1));
        assertTrue(family1.addChild(child2));
        assertEquals(0,family1.getIndexChild(child1));
        assertEquals(1,family1.getIndexChild(child2));

        Human[] childrenBefore = family1.getChildren();
        assertFalse(family1.deleteChild(child3));
        Human[] childrenAfter = family1.getChildren();

        assertArrayEquals(childrenBefore, childrenAfter);

    }

    @Test
    void testDeleteChildByIndex(){
       /* deleteChild(int index) - сделайте 2 проверки:
        проверьте, что ребенок действительно удаляется из массива children и метод возвращает верное значение;
        проверьте, что массив children остается без изменений (если передать индекс, выходящий за диапазон индексов), и метод возвращает верное значение;*/
        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        family1.addChild(child1);
        family1.addChild(child2);

        family1.getIndexChild(child1);
        family1.getIndexChild(child2);

        assertTrue(family1.deleteChild(0));
        assertEquals(-1,family1.getIndexChild(child1));

    }
    @Test
    void testDeleteChildByIndex2(){
       /* deleteChild(int index) - сделайте 2 проверки:
        проверьте, что ребенок действительно удаляется из массива children и метод возвращает верное значение;
        проверьте, что массив children остается без изменений (если передать индекс, выходящий за диапазон индексов), и метод возвращает верное значение;*/
        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        family1.addChild(child1);
        family1.addChild(child2);

        family1.getIndexChild(child1);
        family1.getIndexChild(child2);

        Human[] childrenBefore = family1.getChildren();
        assertFalse(family1.deleteChild(2));
        Human[] childrenAfter = family1.getChildren();

        assertArrayEquals(childrenBefore, childrenAfter);
    }
    @Test
    void testAddChild(){
        /*addChild - проверьте, что массив children увеличивается на один элемент и что этим элементом есть именно переданный объект с необходимыми ссылками;*/
        Human child1 = new Human("Sasha","First",2000);

        //массив children увеличивается на один элемент
        Human[] childrenBefore = family1.getChildren();
        assertEquals(0, childrenBefore.length);
        family1.addChild(child1);
        Human[] childrenAfter = family1.getChildren();
        assertEquals(1,childrenAfter.length);

        //этим элементом есть именно переданный объект с необходимыми ссылками
        Human child2 = family1.getChild(0);
        assertSame(child1, child2);
    }
    @Test
    void testCountFamily(){
        //countFamily - проверьте, что метод возвращает верное количество человек в семье.
        assertEquals(2,family1.countFamily());
        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        family1.addChild(child1);
        family1.addChild(child2);
        assertEquals(4, family1.countFamily());
    }

    @Test
    void testToString(){

        assertEquals("Family{\n " +
                "mother=HUMAN{NAME=LARA, SURNAME=KROFT, YEAR=1990, IQ=0, SCHEDULE=NULL},\n " +
                "father=HUMAN{NAME=PAUL, SURNAME=VOLCOV, YEAR=1980, IQ=0, SCHEDULE=NULL},\n " +
                "children=[],\n  " +
                "pet=NULL }",  family1.toString());
    }

    @Test
    public void testEquals_Symmetric(){
        Human father2 = new Human("Paul","Volcov",1980);
        Human mother2 = new Human("Lara","Kroft",1990);
        Family family2 = new Family(mother2, father2);

        assertEquals(family1.hashCode(), family2.hashCode());
        assertTrue(family1.equals(family2) && family2.equals(family1));

    }
    @Test
    public void testEquals_Symmetric2(){
        Human father2 = new Human("Paul","Volcov",1980);
        Human mother2 = new Human("Lara","Kroft",1990);
        Family family2 = new Family(mother2, father2);

        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        family1.addChild(child1);
        family1.addChild(child2);
        Human child3 = new Human("Sasha","First",2000);
        Human child4 = new Human("Sasha2","Second",2002);
        family2.addChild(child3);
        family2.addChild(child4);

        assertEquals(family1.hashCode(), family2.hashCode());
        assertTrue(family1.equals(family2) && family2.equals(family1));
    }
    @Test
    public void testEquals_NotSymmetric1(){
        Human father2 = new Human("Paul","Volcov",1980);
        Human mother2 = new Human("Lara","Kroft",1990);
        Family family2 = new Family(mother2, father2);

        Human child1 = new Human("Sasha","First",2000);
        Human child2 = new Human("Sasha2","Second",2002);
        family1.addChild(child1);
        family1.addChild(child2);
        Human child3 = new Human("Sasha","First",2000);
        Human child4 = new Human("Sasha2","Second",2002);
        family2.addChild(child3);
        family2.addChild(child4);

        Pet pet = new Pet(Species.DOG,"Sharik",5,50,new String[]{"Sleep","Eat"});
        family1.setPet(pet);

        assertTrue(family1.hashCode() != family2.hashCode());
        assertTrue(!family1.equals(family2) && !family2.equals(family1));
    }

}