package homework06;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpeciesTest {

    @Test
    void testGetValue() {
        Pet pet1 = new DomesticCat();
        Pet pet2 = new Dog();
        Pet pet3 = new Fish();
        assertEquals(Species.DOMESTIC_CAT, Species.getValue(pet1));
        assertEquals(Species.DOG, Species.getValue(pet2));
        assertEquals(Species.FISH, Species.getValue(pet3));
    }
}