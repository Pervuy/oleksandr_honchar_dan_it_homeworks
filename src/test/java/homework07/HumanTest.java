package homework07;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    Human man;
    Human man2;
    Human woman;
    Human woman2;
    Family family;

    @BeforeEach
    void setupThis() {
        // использую TreeMap, чтоб добится четкого порядка для вывода schedule в toString,
        // так как просто Map этого не гарантирует
        man = new Man("Paul", "Volcov", 1980, 90, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        man2 = new Man("Paul", "Fox", 1950);

        woman = new Woman("Lara", "Kroft", 1990, 75, new TreeMap<>(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk")));
        woman2 = new Woman("Svetlana", "Fox", 1960);

        family = new Family(woman2, man2);

        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Pet fish = new Fish("Gold fish", 30, 0, Set.of("Float, Eat"));
        Pet cat = new DomesticCat("Gavrykha", 2, 60, Set.of("Run, Eat, Bite"));
        Set<Pet> pets = Set.of(dog, fish, cat);
        family.setPet(pets);
    }
    @AfterEach
    void afterThis(){
        man = null;
        man2 = null;
        woman = null;
        woman2 = null;
        family = null;
    }

    @Test
    void testFeedPet() {
        Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));

        assertFalse(man.feedPet(false, dog, 30));
        assertTrue(man.feedPet(false, dog, 29));
        assertTrue(man.feedPet(true, dog, 30));
        assertTrue(man.feedPet(true, dog, 29));
    }

    @Test
    void testToString() {

        assertEquals("Man{name=Paul, surname=Volcov, year=1980, iq=90, schedule={MONDAY=Walk, FRIDAY=Sleep}}",  man.toString());
        assertEquals("Man{name=Paul, surname=Fox, year=1950, iq=0, schedule={}}",  man2.toString());

        assertEquals("Woman{name=Lara, surname=Kroft, year=1990, iq=75, schedule={MONDAY=Walk, FRIDAY=Sleep}}",  woman.toString());
        assertEquals("Woman{name=Svetlana, surname=Fox, year=1960, iq=0, schedule={}}",  woman2.toString());
    }

    @Test
    void testGetSurname() {
        assertEquals("Volcov",man.getSurname());
        assertEquals("Kroft",woman.getSurname());
    }

    @Test
    void testGetName() {
        assertEquals("Paul",man.getName());
        assertEquals("Lara",woman.getName());
    }

    @Test
    void testGetYear() {
        assertEquals(1980,man.getYear());
        assertEquals(1990,woman.getYear());
    }

    @Test
    void testGetIq() {
        assertEquals(90,man.getIq());
        assertEquals(75,woman.getIq());
        assertEquals(0,man2.getIq());
    }

    @Test
    void testGetSchedule() {
        assertEquals(Map.of(DayOfWeek.FRIDAY, "Sleep", DayOfWeek.MONDAY, "Walk"),man.getSchedule());
        assertEquals(new TreeMap<>(),man2.getSchedule());
    }

    @Test
    void getFamily() {
        assertEquals(family, man2.getFamily());
        assertEquals(family, woman2.getFamily());
    }
    @Test
    public void testEquals_Symmetric(){
        Man man3 = new Man();
        Man man4 = new Man();

        assertEquals(man3.hashCode(), man4.hashCode());
        assertTrue(man3.equals(man4) && man4.equals(man3));

    }
    @Test
    public void testEquals_Symmetric2(){
        Human man3 = new Man("Paul", "Volcov", 1980);

        assertEquals(man.hashCode(), man3.hashCode());
        assertTrue(man.equals(man3) && man3.equals(man));
    }
    @Test
    public void testEquals_NotSymmetric1(){

        Human man3 = new Man("Paul","Volcov",1988);

        assertTrue(man.hashCode() != man3.hashCode());
        assertTrue(!man.equals(man3) && !man3.equals(man));
    }

}