package homework08.service;

import homework08.Family;
import homework08.Human;
import homework08.Man;
import homework08.Woman;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class FamilyServiceTest {

    Family family1;
    Family family2;
    Family family3;
    Family family4;
    Family family5;
    Family family6;

    FamilyService familyService;
    @BeforeEach
    void setupThis() {

        Human man1 = new Man("Paul", "Volcov", "01/01/1980", 90);
        Human woman1 = new Woman("Lara", "Volcov", "22/05/1990", 75);
        Human child1 = new Man("Sasha","Volcov","05/05/2015");
        Human child2 = new Woman("Lena","Volcov","05/07/2021");

        family1 = new Family(woman1, man1);
        family1.addChild(child1);
        family1.addChild(child2);

        Human man2 = new Man("Alex", "Fox", "04/06/1950",80);
        Human woman2 = new Woman("Svetlana", "Fox", "31/12/1960",90);
        Human child3 = new Man("Sasha","Fox","07/04/1981");
        Human child4 = new Woman("Lena","Fox","04/06/1985");
        Human child5 = new Woman("Lara","Fox","04/07/1989");
        family2 = new Family(woman2, man2);
        family2.addChild(child3);
        family2.addChild(child4);
        family2.addChild(child5);

        Human man3 = new Man("Andrey", "Ivanov", "01/01/1977", 90);
        Human woman3 = new Woman("Masha", "Ivanov", "22/05/1977", 90);
        Human child6 = new Man("Sasha","Ivanov","05/05/1997");
        Human child7 = new Woman("Lena","Ivanov","05/07/2000");

        family3 = new Family(woman3, man3);
        family3.addChild(child6);
        family3.addChild(child7);

        Human man4 = new Man("Mihail", "Selegey", "01/01/1989", 100);
        Human woman4 = new Woman("Nika", "Selegey", "22/05/1988", 100);

        family4 = new Family(woman4, man4);

        Human man5 = new Man("Dima", "Popov", "01/01/2000", 60);
        Human woman5 = new Woman("Katia", "Popov", "22/05/2003", 50);
        Human child8 = new Man("Mihail","Popov","05/05/2020");

        family5 = new Family(woman5, man5);
        family5.addChild(child8);

        Human man6 = new Man("Vasia", "Rogov", "01/01/1985", 70);
        Human woman6 = new Woman("Katia", "Rogov", "22/05/1985", 50);
        Human child9 = new Man("Max","Rogov","05/05/2020");
        Human child10 = new Man("Fax","Rogov","05/05/2022");
        Human child11 = new Man("Pax","Rogov","05/05/2017");
        Human child12 = new Man("Tax","Rogov","05/05/2010");

        family6 = new Family(woman6, man6);
        family6.addChild(child9);
        family6.addChild(child10);
        family6.addChild(child11);
        family6.addChild(child12);
        familyService = new FamilyService();
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        familyService.saveFamily(family4);
        familyService.saveFamily(family5);
        familyService.saveFamily(family6);
        /*Pet dog = new Dog("Rokki", 5, 30, Set.of("Sleep, Eat, Bark"));
        Pet fish = new Fish("Gold fish", 30, 0, Set.of("Float, Eat"));
        Pet cat = new DomesticCat("Gavrykha", 2, 60, Set.of("Run, Eat, Bite"));
        Set<Pet> pets = new TreeSet<>(Set.of(dog,fish,cat));
        family1.setPets(pets);*/
    }
    @AfterEach
    void afterThis(){

        family1 = null;
        family2 = null;
        family3 = null;
        family4 = null;
        family5 = null;
        family6 = null;
        familyService = null;
    }
    @Test
    void testGetAllFamilies() {
        List<Family> families = List.of(family1, family2, family3, family4, family5, family6);
        assertEquals(families, familyService.getAllFamilies());

        ArrayList<Family> families1 = new ArrayList<>();
        List<Family> allFamilies = new FamilyService().getAllFamilies();
        assertEquals(families1, allFamilies);
    }

    @Test
    void testGetFamiliesBiggerThan() {
        List<Family> families = List.of(family1, family2, family3, family4, family5, family6);
        assertEquals(families, familyService.getFamiliesBiggerThan(1));
        List<Family> families2 = List.of(family6);
        assertEquals(families2, familyService.getFamiliesBiggerThan(5));
        List<Family> families3 = new ArrayList<>();
        assertEquals(families3, familyService.getFamiliesBiggerThan(6));

    }

    @Test
    void testGetFamiliesLessThan() {
        List<Family> families = List.of(family1, family2, family3, family4, family5, family6);
        assertEquals(families, familyService.getFamiliesLessThan(7));
        List<Family> families2 = List.of(family4);
        assertEquals(families2, familyService.getFamiliesLessThan(3));
        List<Family> families3 = new ArrayList<>();
        assertEquals(families3, familyService.getFamiliesLessThan(2));
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        assertEquals(2, familyService.countFamiliesWithMemberNumber(4));
        assertEquals(1, familyService.countFamiliesWithMemberNumber(6));
        assertEquals(0, familyService.countFamiliesWithMemberNumber(10));
    }

    @Test
    void testDeleteFamilyByIndex() {
        assertTrue(familyService.deleteFamilyByIndex(0));
        assertFalse(familyService.deleteFamilyByIndex(6));
    }

    @Test
    void testBornChild() {


    }

    @Test
    void testAdoptChild() {

        Human child = new Human("Paul", "Volcov", "01/12/2005", 50);
        int countBefore = family1.countFamily();
        Family family = familyService.adoptChild(family1, child);
        int countAfter = family1.countFamily();

        assertTrue(familyService.getAllFamilies().contains(family));
        assertTrue(family.getChildren().contains(child));
        assertEquals(countBefore+1, countAfter);

        //второй раз тот же ребенок не добавляется
        Family family7 = familyService.adoptChild(family1, child);
        int countAfter2 = family7.countFamily();
        assertEquals(countAfter, countAfter2);

    }

    @Test
    void testCount() {

        assertEquals(6,familyService.count());
    }

    @Test
    void testGetFamilyById() {
        assertEquals(family1,familyService.getFamilyById(0));
        assertEquals(family6,familyService.getFamilyById(5));
        assertNull(familyService.getFamilyById(6));
        assertNull(familyService.getFamilyById(-50));

    }

}